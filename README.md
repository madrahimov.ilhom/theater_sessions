# Расписание театра

## Модель
    Schedule - Расписания
Поля:
- name - название
- start_at_end_at - Начало и конец даты. Формат daterange

Для валидации пересечения дат, в таблицу schedules добавлена ораничение для диапазонов.
https://www.postgresql.org/docs/current/rangetypes.html#RANGETYPES-CONSTRAINT

## Сервисы
Для работы с расписаниями добавлены сервисы:
- schedule/create_service.rb
- schedule/list_service.rb
- schedule/list_service.rb

### Создание
    curl --location --request POST 'http://localhost:3000/api/schedules?start_at=2022-01-01&end_at=2022-01-11&name=name'
### Список
    curl --location --request GET 'http://localhost:3000/api/schedules'
### Удаление
    curl --location --request DELETE 'http://localhost:3000/api/schedules/6'

