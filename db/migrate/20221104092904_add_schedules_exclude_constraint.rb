# frozen_string_literal: true

class AddSchedulesExcludeConstraint < ActiveRecord::Migration[7.0]
  def up
    execute("
                  ALTER TABLE schedules
                  ADD CONSTRAINT check_overlap_start_at_end_at
                  EXCLUDE USING gist (start_at_end_at WITH &&);
            ")
  end

  def down
    execute("
                  ALTER TABLE schedules
                  DROP CONSTRAINT check_overlap_start_at_end_at;
            ")
  end
end
