# frozen_string_literal: true

class CreateSchedules < ActiveRecord::Migration[7.0]
  def change
    create_table :schedules do |t|
      t.string :name, null: false
      t.daterange :start_at_end_at, null: false
      t.timestamps
    end
    add_index :schedules, :start_at_end_at, using: :gist
  end
end
