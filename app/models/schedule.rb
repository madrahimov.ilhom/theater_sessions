# frozen_string_literal: true

class Schedule < ApplicationRecord
  validates :name, :start_at_end_at, presence: true
end
