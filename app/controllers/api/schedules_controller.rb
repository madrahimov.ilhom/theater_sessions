# frozen_string_literal: true

module Api
  class SchedulesController < ApiController
    def index
      service = Schedules::ListService.call
      render json: Schedules::ListSerializer.new(service.result), status: :ok
    end

    def create
      service = Schedules::CreateService.call(params)

      if service.success?
        render json: Schedules::ShowSerializer.new(service.result)
                                              .serializable_hash,
               status: :created
      else
        render json: {
          errors: service.errors.messages
        }, status: :unprocessable_entity
      end
    end

    def destroy
      service = Schedules::DestroyService.call(params)

      if service.success?
        render json: {
          data: {}
        }, status: :ok
      else
        render json: {
          errors: service.errors.messages
        }, status: :unprocessable_entity
      end
    end
  end
end
