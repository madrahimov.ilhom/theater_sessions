# frozen_string_literal: true

module Schedules
  class ListSerializer
    include JSONAPI::Serializer
    attributes :name

    attribute :start_at do |object|
      object.start_at_end_at.first
    end

    attribute :end_at do |object|
      object.start_at_end_at.last
    end
  end
end
