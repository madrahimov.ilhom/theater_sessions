# frozen_string_literal: true

class ApplicationService
  extend ActiveModel::Naming

  attr_reader :params

  def initialize(params = {})
    @params = params
    errors
  end

  def self.call(params = {})
    new(params).send(:call)
  end

  def process; end

  def success?
    errors.blank?
  end

  def failed?
    !success?
  end

  def valid?
    validate

    success?
  end

  def errors
    @errors ||= ActiveModel::Errors.new(self)
  end

  def read_attribute_for_validation(attr)
    send(attr)
  end

  def self.human_attribute_name(attr, _options = {})
    attr
  end

  def self.lookup_ancestors
    [self]
  end

  private

  def call
    process if errors.blank?

    self
  end
end
