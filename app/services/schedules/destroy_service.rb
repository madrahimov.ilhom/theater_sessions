# frozen_string_literal: true

module Schedules
  class DestroyService < ApplicationService
    def process
      destroy if valid?
    end

    private

    def destroy
      schedule.destroy
    end

    def schedule
      @schedule ||= Schedule.find_by(id: id)
    end

    def validate
      errors.add(:schedule, :not_found) if schedule.blank?
    end

    def id
      @id ||= params[:id]
    end
  end
end
