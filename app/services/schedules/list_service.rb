# frozen_string_literal: true

module Schedules
  class ListService < ApplicationService
    attr_reader :result

    def process
      @result = list
    end

    private

    def list
      Schedule.order(start_at_end_at: :desc).limit(100)
    end
  end
end
