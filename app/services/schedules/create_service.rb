# frozen_string_literal: true

module Schedules
  class CreateService < ApplicationService
    attr_reader :result

    def process
      @result = create if valid?
    end

    private

    def create
      Schedule.create!(
        name: params[:name],
        start_at_end_at: start_at_end_at
      )
    rescue ActiveRecord::StatementInvalid
      @errors.add(:start_at_end_at, :overlap, message: 'date is overlap')
    end

    def validate
      validate_name
      validate_start_at
      validate_end_at
    end

    def validate_name
      errors.add(:name, :blank) if name.blank?
    end

    def validate_start_at
      errors.add(:start_at, :date_format) if start_at.blank?
    end

    def validate_end_at
      errors.add(:end_at, :date_format) if end_at.blank?
    end

    def name
      @name ||= params[:name]
    end

    def start_at_end_at
      (start_at..end_at)
    end

    def start_at
      @start_at ||= to_date(params[:start_at])
    end

    def end_at
      @end_at ||= to_date(params[:end_at])
    end

    def to_date(date)
      date.to_date
    rescue StandardError
      nil
    end
  end
end
