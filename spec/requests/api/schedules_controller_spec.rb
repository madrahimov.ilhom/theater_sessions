# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Api::SchedulesControllers' do
  describe '#index' do
    let(:first_schedule) { create(:schedule, start_at_end_at: first_start_at_end_at) }
    let(:second_schedule) { create(:schedule, start_at_end_at: second_start_at_end_at) }

    let(:first_start_at_end_at) { (('2022-01-01'.to_date)..('2022-01-11'.to_date)) }
    let(:second_start_at_end_at) { (('2022-02-01'.to_date)..('2022-02-11'.to_date)) }

    before do
      first_schedule
      second_schedule
    end

    it do
      get '/api/schedules'
      json_response = JSON.parse(response.body)

      expect(response).to have_http_status :ok
      expect(json_response['data'].size).to eq(2)
    end
  end

  describe '#create' do
    context 'when valid params' do
      let(:start_at) { '2022-01-01' }
      let(:end_at) { '2022-01-11' }
      let(:params) do
        {
          name: Faker::Name.name,
          start_at: start_at.to_date,
          end_at: end_at.to_date
        }
      end

      it 'created' do
        post '/api/schedules', params: params
        json_response = JSON.parse(response.body)

        expect(response).to have_http_status :created
        expect(json_response.dig('data', 'attributes', 'start_at')).to eq(start_at)
        expect(json_response.dig('data', 'attributes', 'end_at')).to eq(end_at)
      end
    end

    context 'when invalid params' do
      let(:start_at) { '2022-01-01' }
      let(:end_at) { nil }
      let(:params) do
        {
          name: Faker::Name.name,
          start_at: start_at.to_date,
          end_at: nil
        }
      end

      it 'return error' do
        post '/api/schedules', params: params
        json_response = JSON.parse(response.body)

        expect(response).to have_http_status :unprocessable_entity
        expect(json_response.dig('errors', 'end_at')).to eq(['Date format not valid'])
      end
    end

    context 'when date overlap' do
      let(:start_at) { '2022-01-01' }
      let(:end_at) { '2022-01-11' }
      let(:params) do
        {
          name: Faker::Name.name,
          start_at: start_at.to_date + 1.day,
          end_at: end_at.to_date - 1.day
        }
      end

      let(:create_schedule) { create(:schedule, start_at_end_at: start_at_end_at) }
      let(:start_at_end_at) { (start_at.to_date..end_at.to_date) }

      before do
        create_schedule
      end

      it 'return error' do
        post '/api/schedules', params: params

        json_response = JSON.parse(response.body)

        expect(response).to have_http_status :unprocessable_entity
        expect(Schedule.count).to eq(1)
        expect(json_response.dig('errors', 'start_at_end_at')).to eq(['date is overlap'])
      end
    end
  end

  describe '#destroy' do
    context 'when valid params' do
      let(:first_schedule) { create(:schedule, start_at_end_at: first_start_at_end_at) }
      let(:second_schedule) { create(:schedule, start_at_end_at: second_start_at_end_at) }

      let(:first_start_at_end_at) { (('2022-01-01'.to_date)..('2022-01-11'.to_date)) }
      let(:second_start_at_end_at) { (('2022-02-01'.to_date)..('2022-02-11'.to_date)) }

      before do
        first_schedule
        second_schedule
      end

      it 'destroyed' do
        delete "/api/schedules/#{first_schedule.id}"

        expect(response).to have_http_status :ok
        expect(Schedule.all.count).to eq(1)
      end
    end

    context 'when not found' do
      let(:first_schedule) { create(:schedule, start_at_end_at: first_start_at_end_at) }
      let(:second_schedule) { create(:schedule, start_at_end_at: second_start_at_end_at) }

      let(:first_start_at_end_at) { (('2022-01-01'.to_date)..('2022-01-11'.to_date)) }
      let(:second_start_at_end_at) { (('2022-02-01'.to_date)..('2022-02-11'.to_date)) }

      before do
        first_schedule
        second_schedule
      end

      it 'return error' do
        delete '/api/schedules/str'

        expect(response).to have_http_status :unprocessable_entity
        expect(Schedule.all.count).to eq(2)
      end
    end
  end
end
