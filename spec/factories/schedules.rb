# frozen_string_literal: true

FactoryBot.define do
  factory :schedule do
    name { Faker::Name.name }
  end
end
