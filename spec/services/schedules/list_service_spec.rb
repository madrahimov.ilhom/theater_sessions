# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Schedules::ListService do
  describe '#process' do
    subject(:call_service) { described_class.call }

    context 'when valid' do
      let(:first_schedule) { create(:schedule, start_at_end_at: first_start_at_end_at) }
      let(:second_schedule) { create(:schedule, start_at_end_at: second_start_at_end_at) }

      let(:first_start_at_end_at) { (('2022-01-01'.to_date)..('2022-01-11'.to_date)) }
      let(:second_start_at_end_at) { (('2022-02-01'.to_date)..('2022-02-11'.to_date)) }

      before do
        first_schedule
        second_schedule
      end

      it 'success' do
        call_service
        result = call_service.result
        expect(result.count).to eq(2)
      end
    end

    context 'when empty' do
      it 'empty' do
        call_service
        result = call_service.result
        expect(result.count).to eq(0)
      end
    end
  end
end
