# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Schedules::CreateService do
  describe '#process' do
    subject(:call_service) { described_class.call(params) }

    context 'when valid' do
      let(:start_at) { '2022-01-01' }
      let(:end_at) { '2022-01-11' }
      let(:params) do
        {
          name: Faker::Name.name,
          start_at: start_at.to_date,
          end_at: end_at.to_date
        }
      end

      it 'success' do
        call_service
        result = call_service.result
        expect(call_service.success?).to be(true)
        expect(result.start_at_end_at.first).to eq(start_at.to_date)
        expect(result.start_at_end_at.last).to eq(end_at.to_date)
      end
    end

    context 'when error' do
      let(:start_at) { '2022-01-01' }
      let(:end_at) { '2022-01-11' }
      let(:params) do
        {
          name: Faker::Name.name,
          start_at: start_at.to_date,
          end_at: nil
        }
      end

      it 'failed' do
        call_service
        errors = call_service.errors
        expect(call_service.success?).to be(false)
        expect(errors.messages[:end_at]).to eq(['Date format not valid'])
      end
    end

    context 'when overlap' do
      let(:start_at) { '2022-01-01' }
      let(:end_at) { '2022-01-11' }
      let(:params) do
        {
          name: Faker::Name.name,
          start_at: start_at.to_date + 1.day,
          end_at: end_at.to_date - 1.day
        }
      end

      let(:create_schedule) { create(:schedule, start_at_end_at: start_at_end_at) }
      let(:start_at_end_at) { (start_at.to_date..end_at.to_date) }

      before do
        create_schedule
      end

      it 'failed' do
        call_service
        errors = call_service.errors
        expect(call_service.success?).to be(false)
        expect(errors.messages[:start_at_end_at]).to eq(['date is overlap'])
      end
    end
  end
end
