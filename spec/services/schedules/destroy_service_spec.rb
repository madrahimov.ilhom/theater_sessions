# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Schedules::DestroyService do
  describe '#process' do
    subject(:call_service) { described_class.call(params) }

    let(:first_schedule) { create(:schedule, start_at_end_at: first_start_at_end_at) }
    let(:second_schedule) { create(:schedule, start_at_end_at: second_start_at_end_at) }

    let(:first_start_at_end_at) { (('2022-01-01'.to_date)..('2022-01-11'.to_date)) }
    let(:second_start_at_end_at) { (('2022-02-01'.to_date)..('2022-02-11'.to_date)) }

    context 'when valid' do
      let(:params) { { id: first_schedule.id } }

      before do
        first_schedule
        second_schedule
      end

      it 'destroy' do
        call_service
        expect(call_service.success?).to be(true)
        expect(Schedule.all.count).to eq(1)
      end
    end

    context 'when invalid params' do
      let(:params) { { id: nil } }

      it 'error' do
        call_service
        errors = call_service.errors
        expect(call_service.success?).to be(false)
        expect(errors.messages[:schedule]).to eq(['Schedule not found'])
      end
    end
  end
end
