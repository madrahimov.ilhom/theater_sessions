# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Schedule do
  let(:create_schedule) { create(:schedule, start_at_end_at: start_at_end_at) }
  let(:start_at_end_at) { (('2022-01-01'.to_date)..('2022-01-11'.to_date)) }

  describe 'created' do
    it do
      schedule = create_schedule
      expect(schedule.start_at_end_at).to eq(start_at_end_at)
    end
  end
end
